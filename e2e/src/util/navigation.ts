import { browser } from 'protractor';

export const navigateTo = async (url: string): Promise<unknown> => {
	return browser.get(url);
};
