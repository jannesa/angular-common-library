import { browser, logging } from 'protractor';

export const getConsoleLogs = async (): Promise<logging.Entry[]> => {
	return await browser.manage().logs().get(logging.Type.BROWSER);
};
