import { HomePage } from './home.po';
import { logging } from 'protractor';
import { getConsoleLogs } from '../util';

describe('Home', () => {
	const page = new HomePage();

	it('should home main page', async () => {
		await page.navigateTo();
		expect(await page.getContentText()).not.toEqual('App main title');
		expect(await page.getContentText()).toEqual('Welcome to home page!');
	});

	afterEach(async () => {
		// Assert that there are no errors emitted from the browser
		const logs = await getConsoleLogs();
		expect(logs).not.toContain(jasmine.objectContaining({
			level: logging.Level.SEVERE,
		} as logging.Entry));
	});
});
