import { by, element } from 'protractor';
import { navigateTo } from '../util';

export class HomePage {
	async navigateTo(): Promise<unknown> {
		return navigateTo('/home');
	}

	async getTitleText(): Promise<string> {
		return element(by.css('app-root h1')).getText();
	}

	async getContentText(): Promise<string> {
		return element(by.css('app-root #content p')).getText();
	}
}
