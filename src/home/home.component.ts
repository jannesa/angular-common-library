import { Component, OnInit } from '@angular/core';
import { WidgetItem } from '../widgets/widget-item';
import { WidgetService } from '../widgets/widget.service';

@Component({
	selector: 'lib-home',
	template: `
		<p>Welcome to home page!</p>
		<lib-widget-container *ngFor="let widget of widgets" [widget]="widget"></lib-widget-container>
	`,
})
export class HomeComponent implements OnInit {

	widgets: WidgetItem[] = [];

	constructor(private widgetService: WidgetService) {}

	ngOnInit() {
		this.widgets = this.widgetService.getWidgets();
	}
}
