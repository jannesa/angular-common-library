import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { WidgetModule } from '../widgets/widget.module';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';

export { HomeComponent } from './home.component';

@NgModule({
	imports: [
		CommonModule,
		HomeRoutingModule,
		WidgetModule,
	],
	declarations: [
		HomeComponent,
	]
})
export class HomeModule { }
