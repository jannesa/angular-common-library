import { NgModule } from '@angular/core';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';

export { AdminComponent } from './admin.component';

@NgModule({
	imports: [
		AdminRoutingModule,
	],
	declarations: [
		AdminComponent,
	]
})
export class AdminModule { }
