import { Component } from '@angular/core';

@Component({
	selector: 'lib-admin',
	template: `<p>This is an admin page.<p>`,
})
export class AdminComponent {}
