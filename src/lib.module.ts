import { NgModule } from '@angular/core';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { ModuleTranslateLoader, IModuleTranslationOptions } from '@larscom/ngx-translate-module-loader';

import { CommonLibraryRoutingModule } from './lib-routing.module';
import { LayoutModule } from './layout';
import { AdminModule } from './admin';
import { HomeModule } from './home';
import { WidgetModule } from './widgets';

export function moduleHttpLoaderFactory(http: HttpClient) {
	const baseTranslateUrl = './assets/i18n';

	const options: IModuleTranslationOptions = {
		deepMerge: true,
		disableNamespace: true,
		modules: [
			{
				baseTranslateUrl,
				pathTemplate: '{baseTranslateUrl}/lib_{language}',
			},
			{
				baseTranslateUrl,
				pathTemplate: '{baseTranslateUrl}/app_{language}',
			}
		]
	};

	return new ModuleTranslateLoader(http, options);
}

@NgModule({
	imports: [
		LayoutModule,
		CommonLibraryRoutingModule,
		HomeModule,
		AdminModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: moduleHttpLoaderFactory,
				deps: [HttpClient]
			}
		})
	],
	exports: [
		LayoutModule,
		CommonLibraryRoutingModule,
		HomeModule,
		AdminModule,
		WidgetModule,
	],
})
export class CommonLibraryModule {}
