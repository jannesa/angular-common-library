import { Injectable } from '@angular/core';

import { ImageWidgetComponent } from './image/image-widget.component';
import { TitleWidgetComponent } from './title/title-widget.component';
import { WidgetItem } from './widget-item';

@Injectable()
export class WidgetService {

	private readonly widgets: WidgetItem[] = [
		new WidgetItem(TitleWidgetComponent, { title: 'Widget title', body: 'Lorem ipsum' }),
		new WidgetItem(ImageWidgetComponent, { imageUrl: 'http://example.com/image.png' }),
	];

	getWidgets() {
		return this.widgets;
	}

	addWidget(widget: WidgetItem) {
		this.widgets.push(widget);
	}

}
