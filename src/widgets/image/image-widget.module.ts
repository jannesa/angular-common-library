import { NgModule } from "@angular/core";
import { ImageWidgetComponent } from "./image-widget.component";

@NgModule({
	imports: [],
	declarations: [
		ImageWidgetComponent,
	],
})
export class ImageWidgetModule { }
