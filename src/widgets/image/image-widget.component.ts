import { Component } from '@angular/core';
import { WidgetComponent } from '@common-library/widgets';

@Component({
	template: `
		<div class="image-widget">
			<pre>{{ data.imageUrl }}</pre>
		</div>
	`
})
export class ImageWidgetComponent implements WidgetComponent {
	data: any;
}
