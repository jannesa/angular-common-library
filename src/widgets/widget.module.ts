import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { WidgetDirective } from './widget-host.directive';
import { WidgetService } from './widget.service';
import { WidgetContainerComponent } from './widget-container.component';
import { ImageWidgetModule } from './image';
import { TitleWidgetModule } from './title';

export { WidgetService } from './widget.service';
export { WidgetContainerComponent } from './widget-container.component';
export { WidgetItem } from './widget-item';
export { WidgetComponent } from './widget.component';

@NgModule({
	imports: [
		BrowserModule,
		ImageWidgetModule,
		TitleWidgetModule,
	],
	providers: [
		WidgetService
	],
	declarations: [
		WidgetContainerComponent,
		WidgetDirective,
	],
	exports: [
		WidgetContainerComponent,
	]
})
export class WidgetModule { }
