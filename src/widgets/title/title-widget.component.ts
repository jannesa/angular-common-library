import { Component } from '@angular/core';
import { WidgetComponent } from '@common-library/widgets';

@Component({
	template: `
		<div class="title-widget">
			<h4>{{data.title}}</h4>

			{{data.body}}
		</div>
	`
})
export class TitleWidgetComponent implements WidgetComponent {
	data: any;
}
