import { NgModule } from "@angular/core";
import { TitleWidgetComponent } from "./title-widget.component";

@NgModule({
	imports: [],
	declarations: [
		TitleWidgetComponent,
	],
})
export class TitleWidgetModule { }
