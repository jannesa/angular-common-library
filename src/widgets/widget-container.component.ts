import { Component, ViewChild, ComponentFactoryResolver, Input, OnInit } from '@angular/core';
import { WidgetComponent } from './widget.component';
import { WidgetDirective } from './widget-host.directive';
import { WidgetItem } from './widget-item';

@Component({
	selector: 'lib-widget-container',
	template: `
		<div class="widget" style="border: 1px solid black; padding: 0.5rem; margin: 10px;">
			<ng-template libWidgetHost></ng-template>
		</div>
	`
})
export class WidgetContainerComponent implements OnInit {

	@Input() widget: WidgetItem;

	@ViewChild(WidgetDirective, { static: true }) widgetHost: WidgetDirective;

	constructor(
		private componentFactoryResolver: ComponentFactoryResolver) { }

	ngOnInit() {
		this.loadComponent();
	}

	loadComponent() {
		const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.widget.component);

		const viewContainerRef = this.widgetHost.viewContainerRef;
		viewContainerRef.clear();

		const componentRef = viewContainerRef.createComponent<WidgetComponent>(componentFactory);
		componentRef.instance.data = this.widget.data;
	}
}
