import { Component, Input, OnInit } from '@angular/core';
import { HeaderConfig } from './header.config';

@Component({
	selector: 'lib-header',
	template: `
		<h1>{{ title }}</h1>
		<img src="assets/img/logo.png" />
		<p>{{ 'header.subtitle' | translate }}</p>
	`,
})
export class HeaderComponent implements OnInit {

	@Input() title: string;

	constructor(private config: HeaderConfig) { }

	ngOnInit(): void {
		if (!this.title) {
			this.title = this.config.title;
		}
	}

}
