import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class HeaderConfig {

	title: string = 'App main title';

	description: string | null = null;

}
