import { HeaderConfig } from './header.config';

describe('header-config', () => {
	it('should have sensible default values', () => {
		const config = new HeaderConfig();

		expect(config.title).toBe('App main title');
		expect(config.description).toBeNull();
	});
});
