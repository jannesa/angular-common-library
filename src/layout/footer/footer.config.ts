import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class FooterConfig {

	version: string = '0.0.0';
}
