import { Component } from '@angular/core';
import { FooterConfig } from './footer.config';

@Component({
	selector: 'lib-footer',
	templateUrl: './footer.component.html',
})
export class FooterComponent {

	constructor(public config: FooterConfig) { }

	get year() {
		return new Date().getFullYear();
	}

}
