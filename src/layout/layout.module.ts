import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { FooterComponent } from './footer';
import { HeaderComponent } from './header';

@NgModule({
	imports: [
		CommonModule,
		TranslateModule,
	],
	declarations: [
		HeaderComponent,
		FooterComponent,
	],
	exports: [
		HeaderComponent,
		FooterComponent,
	],
})
export class LayoutModule { }
