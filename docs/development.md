# Development

To watch for changes with the library use `yarn start` command. In practice this is useless because watching library changes independently won't show any meaningful feedback. For more meaningful development see options below.


## Test-driven-development

`yarn tdd` starts karma testing while watching changes in library code.


## Developing with application

The library and the application together can be watched using `yarn start` command in application's root.


### Explanation

Use `rimraf` to clean the dist directory, `wait-on` to await the building of the library and `npm-run-all` to run the watch scripts parallel with one command.

Scripts in application's `package.json` explained:

```json
"lib:clean": "rimraf common-library/dist"
```

Removes the library's dist folder.

```json
"app:start": "wait-on common-library/dist/fesm2015 && app:serve"
```

Waits on the `fesm2015` folder in the dist directory, `app:serve` starts the app.

```json
"lib:watch": "ng build common-library --watch"
```

Builds the library in watch mode.

```json
"watch:all": "yarn lib:clean && run-p lib:watch app:start"
```

Cleans the dist folder, after that parallelly serves the application and watches the library.
