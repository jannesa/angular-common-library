# Installation

Create new Angular application.

## Installing the library as Git submodule

Use command below to add Git submodule into your application's repository.

```bash
git submodule add <common-library-git-link> common-library
```

Install library's dependencies.

```bash
yarn --cwd common-library
```


## Modifying the application

### Dependencies

Install dependencies for application:

General:

```bash
yarn add --dev @angular-devkit/build-ng-packagr ng-packagr
```

Development:

```bash
yarn add --dev npm-run-all rimraf wait-on
```

Analyzing:

```bash
yarn add --dev http-server source-map-explorer
```

Testing:

```bash
yarn add --dev @types/jasmine codelyzer jasmine-core jasmine-spec-reporter karma karma-chrome-launcher karma-coverage karma-jasmine karma-jasmine-html-reporter protractor tsconfig-paths
```


### TypeScript compiler paths

Add `extends` option into application's `tsconfig.json`. With this library modules and files can be imported from `common-library`.

```json
// tsconfig.json

{
	"extends": "common-library/tsconfig.paths.json",
	"compilerOptions": {
		...
	}
}
```


### Angular.json

Add following project configuration into app's `angular.json`. With this Angular will be able to build library within same workspace easily.

```json
// angular.json

{
	"projects": {
		"my-application": {},
		"common-library": {
			"projectType": "library",
			"root": "common-library",
			"sourceRoot": "common-library/src",
			"architect": {
				"build": {
					"builder": "@angular-devkit/build-ng-packagr:build",
					"options": {
						"tsConfig": "common-library/src/tsconfig.json",
						"project": "common-library/src/ng-package.json"
					},
					"configurations": {
						"production": {
							"tsConfig": "common-library/src/tsconfig.prod.json"
						}
					}
				}
			}
		}
	},
	"cli": {
		"packageManager": "yarn",
		"analytics": false
	}
}
```


### NPM/Yarn scripts

Add or replace following scripts in `package.json` with scripts below.

```json
// package.json

{
	"scripts": {
		"ng": "ng",
		"start": "yarn watch:all",
		"build": "yarn lib:build && yarn app:build",

		"test": "ng test",
		"lint": "ng lint",
		"e2e": "ng e2e",

		"analyze": "source-map-explorer dist/main.*.js",
		"serve": "yarn http-server dist -o",

		"app:start": "wait-on common-library/dist/fesm2015 && yarn app:serve",
		"app:serve": "ng serve custom-project --poll 2000",
		"app:build": "ng build custom-project --prod",

		"lib:clean": "rimraf common-library/dist",
		"lib:watch": "ng build common-library --watch",
		"lib:build": "ng build common-library --prod",

		"watch:all": "yarn lib:clean && run-p lib:watch app:start"
	}
}
```


### Importing library module

Import library module into `AppModule`.

```ts
// src/app/app.module.ts

import { CommonLibraryModule } from 'common-library';

@NgModule({
	imports: [
		// ...
		CommonLibraryModule,
	],
})
export class AppModule { }
```


## Setup testing

### Including library test specs in application

In order to TypeScript paths to work `protractor.conf.js` file must register `tsconfig-paths` module by adding require on top of the file.

```ts
// e2e/protractor.conf.js

require('tsconfig-paths/register');
```

Add path to the library e2e specifications into `specs` array in `protractor.conf.js` file.

```ts
// e2e/protractor.conf.js

specs: [
	// ...
	'../common-library/e2e/src/**/*.e2e-spec.ts'
],
```
