# Building

## Building the library

To build the library execute `yarn build` command. Finished build can be found in library's `dist` folder.


## Building with application

When building with application the library must be compiled first. If installation instructions [here](./installation.md) where followed, run `yarn build` in applications root to build the library and application. Finished build can be found in application's `dist` folder.


### Analyzing the build

Compiled bundle size can be analyzed with `yarn analyze` command.

