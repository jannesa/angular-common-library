# End-to-end tests

This is a library of Angular modules and components. It's not designed to run by itself, therefore running e2e tests for the library are not possible.

However the library has test specifications (`*.spec.ts`) and page objects (`*.po.ts`) which can be used in regular Angular applications.


## Writing e2e tests for library

1. Write page objects for components and pages
	- By writing and using page objects, the objects code can be shared across other tests.
2. Write general tests which don't use assumptions
	- Application e2e test run must also be able to pass with these tests.
3. Export classes and functions within [../e2e/src/index.ts](../e2e/src/index.ts) file


## Using e2e page objects in application tests

Library page objects and e2e util functions can be simply used by importing them from `common-library/testing`.

```ts
import { navigateTo } from 'common-library/testing';
```
