# Troubleshooting

## Unsupported private class

```
× Compiling TypeScript sources through NGC
ERROR: common-library/src/some-module/some-component.component.ts:14:14 - error NG3001: Unsupported private class SomeComponent. This class is visible to consumers via CommonLibraryModule -> SomeModule
-> SomeComponent, but is not exported from the top-level library entrypoint.

14 export class SomeComponent {
```

This error happens if any service, module or component etc. is exported in `NgModule` and is not included in `public-api.ts`.

```
// public-api.ts

export * from './some-module/some.component';
```
